import csv
import pymongo

client = pymongo.MongoClient("localhost", 27017)
database = client["global_climate_change"]
dbCollection = database["GlobalLandTemperatureByCountry"]

with open('GlobalLandTemperaturesByCountry.csv') as csvFile:
    readCsv = csv.reader(csvFile, delimiter=',')
    for row in readCsv:
        data_row = {
            "Date" : row[0],
            "AverageTemperature" : row[1],
            "AverageTemperatureUncertainty" : row[2],
            "Country" : row[3]
        }

        res = dbCollection.insert_one(data_row)
        print("The record inserted from source ", row[0])
        print('..............................................')