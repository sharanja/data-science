const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;

const connectionURL = 'mongodb://localhost:27017';
const databaseName = 'global_climate_change';

mongoClient.connect(connectionURL,
    {useNewUrlParser: true, useUnifiedTopology: true},
    (error, client) => {

        if(error){
            console.log("Database not connected");
            return;
        }

        const database = client.db(databaseName);

        if (database) {
        
            console.log("Database connected successfully");

            database.collection('GlobalLandTemperatureByCountry').find({'AverageTemperature' : /^20/}).toArray((error, GlobalLandTemperatureByCountry) =>{
                GlobalLandTemperatureByCountry.forEach((GlobalLandTemperatureByCountry) => {
                    console.log("*********************************");  
                    console.log("Date : ", GlobalLandTemperatureByCountry.Date);                                        
                    console.log("Average Temperature : ", GlobalLandTemperatureByCountry.AverageTemperature);
                    console.log("Average Temperature Uncertainty : ", GlobalLandTemperatureByCountry.AverageTemperatureUncertainty);
                    console.log("Country : ", GlobalLandTemperatureByCountry.Country);                    

                });
            })


        }


})